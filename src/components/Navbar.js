
import React from 'react';

import '../styles/Navbar.css'
import {
   
    Link
  } from 'react-router-dom'



const Navbar = () => {
    return (
        <div className="nav">
            <h1><Link className="myul-li" to="/">Kodeademia</Link></h1>
            <ul className="myul">
                <li> <Link className="myul-li" to="/">Home</Link></li>
                <li> <Link className="myul-li" to="/about">About</Link></li>
                <li> <Link className="myul-li" to="/contact">Contact</Link></li>
            </ul>
        </div>
    );
};




export default Navbar;
