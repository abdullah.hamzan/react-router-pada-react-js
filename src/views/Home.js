
import React, { Component } from 'react'
import { Mainlayout } from '../layouts/Mainlayout'
import '../styles/Home.css'
import {Link} from 'react-router-dom'

export default class Home extends Component {
    state={
        skills:[
            {
                id:1,
                name:'Javascript',
                image:require("../images/js.png")
            },
            {
                id:2,
                name:'React js',
                image:require("../images/react.png")
            },
            {
                id:3,
                name:'Svelt js',
                image:require("../images/svelt.png")
            },
            {
                id:4,
                name:'Vue js',
                image:require("../images/vue.png")
            }
        ]
    }
   

    render() {
        const {skills}=this.state
        return (
            <Mainlayout >
           <div className="home">
            <h1> Page Home </h1>
            
            <div className="card" >
            {
                skills.map(item=>(

                    <div key={item.id} className="card-item"  >
                        <Link to={`detail/${item.id}`}>
                        <img src={item.image.default} alt={item.name}/>
                        </Link>
                        <h3>{item.name}</h3>
                    </div>
                ))
            }
            </div>
           
          
            </div>
                
            </Mainlayout>
        )
    }
}
