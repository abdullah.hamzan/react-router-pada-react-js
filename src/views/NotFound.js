import React, { Component } from 'react'
import { Mainlayout } from '../layouts/Mainlayout'

export default class NotFound extends Component {
    render() {
        return (
            <Mainlayout>
                <div className="container">
                    <h1>Not Found page</h1>
                </div>

            </Mainlayout>
        )
    }
}
