import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Mainlayout } from '../layouts/Mainlayout'
import '../styles/Detail.css'


export default class Detail extends Component {

    state={
        skills:[
            {
                id:1,
                name:'Javascript',
                image:require("../images/js.png"),
                description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            },
            {
                id:2,
                name:'React js',
                image:require("../images/react.png"),
                description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            },
            {
                id:3,
                name:'Svelt js',
                image:require("../images/svelt.png"),
                description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            },
            {
                id:4,
                name:'Vue js',
                image:require("../images/vue.png"),
                description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            }
        ],
        data:"",
    }
  

    componentDidMount(){
        const id=this.props.match.params.id
        const data=this.state.skills.find(item=>item.id===parseInt(id));

        this.setState({data})
    }

    
    render() {

        let {data} = this.state
        console.log("data:",data)
        console.log("image",data.image)
      
        
        return (
            
            <Mainlayout>
                <div className="container">
                    <h1>{data.name}</h1>
                    <div className="card">
                    <img src={data.image && data.image.default} alt={data.name}/>
                    <p>{data.description}</p>
                    </div>
                    <button className="button"><Link to="/" style={{ textDecoration:'none',color:'white' }}>Back</Link></button>
                </div>


            </Mainlayout>
        )
    }
}
