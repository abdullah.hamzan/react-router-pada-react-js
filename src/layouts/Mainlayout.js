
import React from 'react'
import Footer from '../components/Footer'
import Navbar from '../components/Navbar'

export const Mainlayout = (props) => {
    return (
        <React.Fragment>
         <Navbar />
         <div style={{ height:'160px' }}>
        </div>
        {
            props.children
        }

         <Footer /> 
        </React.Fragment>
    )
}
