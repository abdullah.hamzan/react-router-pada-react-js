import React from 'react'

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom'
import About from '../views/About'
import Contact from '../views/Contact'
import Detail from '../views/Detail'
import Home from '../views/Home'
import NotFound from '../views/NotFound'


const Routes = () => {
    return (

        <Router >
        <Switch>
        <Route path="/detail/:id" component={Detail} />
        <Route path="/about" component={About}/>
        <Route path="/contact" component={Contact} />
        <Route path="/" exact component={Home} />
        <Route  component={NotFound}/>
        </Switch>
        </Router>
            
        
    )
}

export default Routes